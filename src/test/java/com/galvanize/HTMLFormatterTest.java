package com.galvanize;

import formatters.HTMLFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HTMLFormatterTest {
    @Test
    public void testClassHTMLFormatterMethodFormat() {
        //arrange
        Booking booking = new Booking("Classroom", "111", "10:30am", "11:00am");
        HTMLFormatter htmlFormatter = new HTMLFormatter();
        String expected = "<dl>\n  <dt>Type</dt><dd>Classroom</dd>\n  <dt>Room Number</dt><dd>111</dd>\n  <dt>Start Time</dt><dd>10:30am</dd>\n  <dt>End Time</dt><dd>11:00am</dd>\n</dl>";
        //act
        String actual = htmlFormatter.format(booking);
        //assert
        assertEquals(expected, actual);
    }
}
