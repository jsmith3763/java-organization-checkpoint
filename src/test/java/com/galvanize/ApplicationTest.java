package com.galvanize;


import formatters.CSVFormatter;
import formatters.Formatter;
import formatters.HTMLFormatter;
import formatters.JSONFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;
    String[] args;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void testSuiteJSON() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
        //arrange
        String expected = "{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}\n";
        String[] params = {"s111-08:30am-11:00am", "json"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testSuiteCSV() {
        //arrange
        String expected = "type,room number,start time,end time\nSuite,111,10:00am,11:00am\n";
        String[] params = {"s111-10:00am-11:00am", "csv"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testSuiteHTML() {
        //arrange
        String expected = "<dl>\n  <dt>Type</dt><dd>Suite</dd>" +
                "\n  <dt>Room Number</dt><dd>111</dd>" +
                "\n  <dt>Start Time</dt><dd>10:00am</dd>" +
                "\n  <dt>End Time</dt><dd>11:00am</dd>" +
                "\n</dl>\n";
        String[] params = {"s111-10:00am-11:00am", "html"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testClassroomJSON() {
        //arrange
        String expected = "{\n" +
                "  \"type\": \"Classroom\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}\n";
        String[] params = {"c111-08:30am-11:00am", "json"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testClassroomCSV() {
        //arrange
        String expected = "type,room number,start time,end time\nClassroom,111,10:00am,11:00am\n";
        String[] params = {"c111-10:00am-11:00am", "csv"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testClassroomHTML() {
        //arrange
        String expected = "<dl>\n  <dt>Type</dt><dd>Classroom</dd>" +
                "\n  <dt>Room Number</dt><dd>111</dd>" +
                "\n  <dt>Start Time</dt><dd>10:00am</dd>" +
                "\n  <dt>End Time</dt><dd>11:00am</dd>" +
                "\n</dl>\n";
        String[] params = {"c111-10:00am-11:00am", "html"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testConferenceRoomJSON() {
        //arrange
        String expected = "{\n" +
                "  \"type\": \"Conference Room\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}\n";
        String[] params = {"r111-08:30am-11:00am", "json"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testConferenceRoomCSV() {
        //arrange
        String expected = "type,room number,start time,end time\nConference Room,111,10:00am,11:00am\n";
        String[] params = {"r111-10:00am-11:00am", "csv"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testConferenceRoomHTML() {
        //arrange
        String expected = "<dl>\n  <dt>Type</dt><dd>Conference Room</dd>" +
                "\n  <dt>Room Number</dt><dd>111</dd>" +
                "\n  <dt>Start Time</dt><dd>10:00am</dd>" +
                "\n  <dt>End Time</dt><dd>11:00am</dd>" +
                "\n</dl>\n";
        String[] params = {"r111-10:00am-11:00am", "html"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }

    @Test
    public void testAuditoriumJSON() {
        //arrange
        String expected = "{\n" +
                "  \"type\": \"Auditorium\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"10:00am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}\n";
        String[] params = {"a111-10:00am-11:00am", "json"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testAuditoriumCSV() {
        //arrange
        String expected = "type,room number,start time,end time\nAuditorium,111,10:00am,11:00am\n";
        String[] params = {"a111-10:00am-11:00am", "csv"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testAuditoriumHTML() {
        //arrange
        String expected = "<dl>\n  <dt>Type</dt><dd>Auditorium</dd>" +
                "\n  <dt>Room Number</dt><dd>111</dd>" +
                "\n  <dt>Start Time</dt><dd>10:00am</dd>" +
                "\n  <dt>End Time</dt><dd>11:00am</dd>" +
                "\n</dl>\n";
        String[] params = {"a111-10:00am-11:00am", "html"};
        //act
        Application.main(params);
        //assert
        assertEquals(expected, outContent.toString());

    }


    @Test
    public void testGetFormatterJSON() {
        //arrange
        Formatter expected = new JSONFormatter();
        //act
        Formatter actual = Application.getFormatter("json");
        //assert
        assertEquals(expected.getClass(), actual.getClass());
    }

    @Test
    public void testGetFormatterCSV() {
        //arrange
        Formatter expected = new CSVFormatter();
        //act
        Formatter actual = Application.getFormatter("csv");
        //assert
        assertEquals(expected.getClass(), actual.getClass());
    }

    @Test
    public void testGetFormatterHTML() {
        //arrange
        Formatter expected = new HTMLFormatter();
        //act
        Formatter actual = Application.getFormatter("html");
        //assert
        assertEquals(expected.getClass(), actual.getClass());
    }

    @Test
    public void testGetRoomType() {
        //arrange
        Booking booking = new Booking("Auditorium", "111", "08:30am", "10:00am");
        String expected = "Auditorium";
        //act
        String actual = booking.getRoomType();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetRoomNumber() {
        //arrange
        Booking booking = new Booking("Auditorium", "111", "08:30am", "10:00am");
        String expected = "111";
        //act
        String actual = booking.getRoomNumber();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetStartTime() {
        //arrange
        Booking booking = new Booking("Auditorium", "111", "08:30am", "10:00am");
        String expected = "08:30am";
        //act
        String actual = booking.getStartTime();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetEndTime() {
        //arrange
        Booking booking = new Booking("Auditorium", "111", "08:30am", "10:00am");
        String expected = "10:00am";
        //act
        String actual = booking.getEndTime();
        //assert
        assertEquals(expected, actual);
    }
}