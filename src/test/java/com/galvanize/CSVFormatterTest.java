package com.galvanize;


import formatters.CSVFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CSVFormatterTest {
    @Test
    public void testClassCVSFormatterMethodFormat(){
        //arrange
        Booking booking = new Booking("Auditorium", "111", "08:30am", "11:00am");
        CSVFormatter csvFormatter = new CSVFormatter();
        String expected = "type,room number,start time,end time\nAuditorium,111,08:30am,11:00am";
        //act
        String actual = csvFormatter.format(booking);
        //assert
        assertEquals(expected, actual);
    }
}
