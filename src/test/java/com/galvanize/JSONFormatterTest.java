package com.galvanize;


import formatters.JSONFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSONFormatterTest {
    @Test
    public void testClassJSONFormatterMethodFormat() {
        //arrange
        Booking booking = new Booking("Conference Room", "111", "08:00am", "11:00am");
        JSONFormatter jsonFormatter = new JSONFormatter();
        String expected = "{\n" +
                "  \"type\": \"Conference Room\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:00am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";
        //act
        String actual = jsonFormatter.format(booking);
        //assert
        assertEquals(expected, actual);
    }
}
