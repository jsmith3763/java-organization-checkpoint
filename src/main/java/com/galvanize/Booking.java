package com.galvanize;

public class Booking {

    private final String roomType;
    private final String roomNumber;
    private final String startTime;
    private final String endTime;

//    enum RoomType {
//        Conference_Room,
//        Suite,
//        Auditorium,
//        Classroom
//    }


    Booking(String roomType, String roomNumber, String startTime, String endTime) {
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Booking parse(String bookingCode) {
        String parsedType;
        switch (bookingCode.charAt(0)) {
            case 's':
//                parsedType = RoomType.Suite;
                parsedType = "Suite";
                break;
            case 'a':
//                parsedType = RoomType.Auditorium;
                parsedType = "Auditorium";
                break;
            case 'c':
//                parsedType = RoomType.Classroom;
                parsedType = "Classroom";
                break;
            default:
//                parsedType = RoomType.Conference_Room;
                parsedType = "Conference Room";
        }
        String[] bookingInfo = bookingCode.split("-", 3);
        return new Booking(parsedType, bookingInfo[0].substring(1), bookingInfo[1], bookingInfo[2]);
    }


    public String getRoomType() {
        return roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

}
