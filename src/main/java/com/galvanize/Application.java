package com.galvanize;

import formatters.CSVFormatter;
import formatters.Formatter;
import formatters.HTMLFormatter;
import formatters.JSONFormatter;

public class Application {


    public static void main(String[] args) {
        String code = args[0];
        String format = args[1];
        Formatter currentFormatter = getFormatter(format);
        Booking currentBooking = Booking.parse(code);

        System.out.println(currentFormatter.format(currentBooking));

    }

    static Formatter getFormatter(String format) {
        switch(format) {
            case "json":
                return new JSONFormatter();
            case "html":
                return new HTMLFormatter();
            default:
                return new CSVFormatter();
        }
    }
}