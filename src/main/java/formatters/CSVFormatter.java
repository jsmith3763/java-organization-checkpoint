package formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    @Override
    public String format(Booking booking) {
        return String.format("type,room number,start time,end time\n%s,%s,%s,%s", booking.getRoomType(), booking.getRoomNumber(), booking.getStartTime(), booking.getEndTime());
    }
}
