package formatters;

import com.galvanize.Booking;

public interface Formatter {
    public String format(Booking booking);
}
